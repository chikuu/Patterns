package patterns.creational.factory;

public class WhiteEggs implements Eggs {

	@Override
	public void boilEgg() {
		System.out.println("Yummy white boiled eggs . . .");
	}
	
}
