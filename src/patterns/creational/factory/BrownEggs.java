package patterns.creational.factory;

public class BrownEggs implements Eggs {

	@Override
	public void boilEgg() {
		System.out.println("Healthy brown boiled eggs . . .");
	}

}
