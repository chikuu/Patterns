package patterns.creational.factory;

public class Demo {
	public static void main(String[] args) {
		
		Hen hen = new Hen();
		Eggs whiteEgg = hen.layEgg("white");
		Eggs brownEgg = hen.layEgg("brown");
		
		whiteEgg.boilEgg();
		brownEgg.boilEgg();
		
	}
}
