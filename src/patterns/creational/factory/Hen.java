package patterns.creational.factory;

public class Hen {
	
	public Eggs layEgg(String color) {
		if (color.equalsIgnoreCase("white"))
			return new WhiteEggs();
		else if (color.equalsIgnoreCase("brown"))
			return new BrownEggs();
		return null;
	}
	
}
