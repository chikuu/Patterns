package patterns.creational.factory;

public interface Eggs {
	void boilEgg();
}
