package patterns.creational.prototype;

public class Diet {
	private String favFood;

	public String getFavFood() {
		return favFood;
	}

	public void setFavFood(String favFood) {
		this.favFood = favFood;
	}

}
