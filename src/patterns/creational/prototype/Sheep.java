package patterns.creational.prototype;

public class Sheep implements Cloneable{

	private String name;
	private Diet diet = new Diet();
	
	public Sheep(String name) {
		this.name = name;
		System.out.println("New Sheep created");
	}
	
	public Sheep getSheepClone() {
		try {
			return (Sheep) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDiet() {
		return diet.getFavFood();
	}

	public void setDiet(String favFood) {
		diet.setFavFood(favFood);
	}
	
}
