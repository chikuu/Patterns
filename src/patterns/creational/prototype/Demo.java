package patterns.creational.prototype;

import java.lang.reflect.InvocationTargetException;

public class Demo {
	public static void main(String[] args) throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {

		Sheep mom = new Sheep("MotherSheep");
		mom.setDiet("Carrot");
		
		System.out.println("Mom's name: " + mom.getName());
		System.out.println("Mom's diet: " + mom.getDiet());
		
		Sheep dolly = mom.getSheepClone();
		dolly.setName("Dolly");
		System.out.println("New sheep cloned");
		
		System.out.println("Dolly's name: " + dolly.getName());
		System.out.println("Dolly's diet: " + dolly.getDiet());
		
		System.out.println("Changing mom's name and diet");
		mom.setName("Oldie");
		mom.setDiet("Grass");
		
		System.out.println("Mom's name: " + mom.getName());
		System.out.println("Mom's diet: " + mom.getDiet());
		
		System.out.println("Dolly's name: " + dolly.getName());
		System.out.println("Dolly's diet: " + dolly.getDiet());

	}
}
