package patterns.creational.singleton;

public class Singleton {
	
	private Singleton() { }
	
	public static Singleton getObject() {
		return object;
	}
	
	public void sayHello() {
		System.out.println("Hello World!! This is "+ this.toString());
	}

	public String getMyname() {
		return myname;
	}

	public void setMyname(String myname) {
		this.myname = myname;
	}

	private static Singleton object = new Singleton();
	private String myname = "noname";
	
}
