package patterns.creational.singleton;

public class Demo {
	public static void main(String[] args) {
		
		Singleton obj1 = Singleton.getObject();
		obj1.sayHello();
		
		Singleton obj2 = Singleton.getObject();
		obj2.sayHello();
		
	}
}
