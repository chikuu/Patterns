package patterns.behavioral.command;

import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.List;

public class CoffeeShop {

	private List<Coffee> completedOrders = new LinkedList<Coffee>();

	public static Waiter callWaiter() {
		return new Waiter("Bob");
	}
	
	protected List<Coffee> processOrder(List<String> orderedItems) {
		
		for (String coffeeName : orderedItems) {
			try {
				String coffeeClassName = this.getClass().getPackage().getName() + "." + coffeeName;
				Coffee newCoffee = (Coffee)Class.forName(coffeeClassName).getDeclaredConstructor().newInstance();
				completedOrders.add(newCoffee);
			} catch (InvocationTargetException | NoSuchMethodException
					| InstantiationException | IllegalAccessException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		return completedOrders;
	}

}
