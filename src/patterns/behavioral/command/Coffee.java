package patterns.behavioral.command;

public abstract class Coffee {
	
	public abstract void haveCoffee();

}
