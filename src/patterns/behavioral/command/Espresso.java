package patterns.behavioral.command;

public class Espresso extends Coffee {
	
	@Override
	public void haveCoffee() {
		System.out.println("Espresso is yummy");
	}

}
