package patterns.behavioral.command;

public class Cappuccino extends Coffee {
	
	@Override
	public void haveCoffee() {
		System.out.println("Cappuccino is yummy");
	}

}
