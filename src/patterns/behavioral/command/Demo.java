package patterns.behavioral.command;

import java.util.List;

public class Demo {

	public static void main(String[] args) {
		Waiter w = CoffeeShop.callWaiter();
		w.addOrder("Cappuccino");
		w.addOrder("Espresso");
		
		List<Coffee> preparedItems = w.placeOrder();
		for (Coffee coffee: preparedItems)
			coffee.haveCoffee();
	}
	
}
