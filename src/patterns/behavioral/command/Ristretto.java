package patterns.behavioral.command;

public class Ristretto extends Coffee {
	
	@Override
	public void haveCoffee() {
		System.out.println("Ristretto is yummy");
	}

}
