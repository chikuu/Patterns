package patterns.behavioral.command;

import java.util.LinkedList;
import java.util.List;

public class Waiter extends CoffeeShop {
	
	private String name;
	private List<String> orderedItems = new LinkedList<String>();

	public Waiter(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void addOrder(String orderName) {
		orderedItems.add(orderName);
	}
	
	public List<Coffee> placeOrder() {
		return processOrder(orderedItems);
	}

}
