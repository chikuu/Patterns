package patterns.behavioral.strategy;

public class Veggie implements Diet {

	@Override
	public void eatFish() {
		System.out.println("No I'm allergic to fishes!!");
	}

	@Override
	public void eatFruits() {
		System.out.println("Wow I love fruits :)");
	}
	
}
