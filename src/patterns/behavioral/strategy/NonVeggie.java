package patterns.behavioral.strategy;

public class NonVeggie implements Diet{

	@Override
	public void eatFish() {
		System.out.println("Mmm Yummy tasty fishes!!");
	}

	@Override
	public void eatFruits() {
		System.out.println("Sorry I don't like fruits");
	}

}
