package patterns.behavioral.strategy;

public class Demo {
	public static void main(String[] args) {

		Bear panda = new Panda();
		Bear grizy = new Grizzly();
		Bear browny = new BrownBear();

		System.out.println("\nFeeding Panda");
		panda.setFoodType(new Veggie());
		panda.eatFish();
		panda.eatFruits();
		
		System.out.println("\nFeeding Grizly");
		grizy.setFoodType(new NonVeggie());
		grizy.eatFish();
		
		System.out.println("\nFeeding BrownBear");
		browny.eatFruits();
		browny.eatFruits();

	}
}
