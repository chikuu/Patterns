package patterns.behavioral.strategy;

public interface Diet {
	void eatFish();
	void eatFruits();
}
