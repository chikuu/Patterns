package patterns.behavioral.strategy;

public class Grizzly extends Bear{
	private final String familyName = "Grizzly";

	public String getFamilyName() {
		return familyName;
	}
}
