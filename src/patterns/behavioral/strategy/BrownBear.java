package patterns.behavioral.strategy;

public class BrownBear extends Bear{
	private final String familyName = "Brown Bear";

	public String getFamilyName() {
		return familyName;
	}
}
