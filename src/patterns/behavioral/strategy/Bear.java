package patterns.behavioral.strategy;

public abstract class Bear implements Diet {
	
	String name = null;
	String color = null;
	Diet foodType = null;
	
	public Bear() {
		System.out.println("I'm a new Bear");
	}
	
	public Diet getFoodType() {
		return foodType;
	}

	public void setFoodType(Diet foodType) {
		this.foodType = foodType;
	}
	
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void speak() {
		System.out.println("I'm so fluffy");
	}

	@Override
	public void eatFish() {
		try {
			this.foodType.eatFish();
		} catch (NullPointerException ex) {
			nonEater();
		}
	}

	@Override
	public void eatFruits() {
		try {
			this.foodType.eatFruits();
		} catch (NullPointerException ex) {
			nonEater();
		}
	}
	
	public void nonEater() {
		System.out.println("I don't know how to eat!! :(");
	}
	
}
