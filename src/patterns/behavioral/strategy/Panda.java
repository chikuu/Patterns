package patterns.behavioral.strategy;

public class Panda extends Bear{
	private final String familyName = "Cute Panda";

	public String getFamilyName() {
		return familyName;
	}
}
