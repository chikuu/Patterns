package patterns.behavioral.observer;


public class FireAlarm extends ThreadyObservable {
	
	String alert;
	
	public void setAlert(String alert) {
		this.alert = alert;
		setChanged();
		notifyObservers(alert);
	}
	
}
