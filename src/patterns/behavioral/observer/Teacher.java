package patterns.behavioral.observer;

import java.util.Observable;
import java.util.Observer;

public class Teacher implements Observer {

	String msg;
	
	@Override
	public void update(Observable o, Object arg) {
		this.msg = (String) arg;
		flyAway();
	}

	private void flyAway() {
		System.out.println(msg);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Teacher is running . . . . . .");
	}

}
