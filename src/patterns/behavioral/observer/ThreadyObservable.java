package patterns.behavioral.observer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;

public class ThreadyObservable extends Observable {
	ArrayList<Observer> observerList = new ArrayList<Observer>();
	
	@Override
	public synchronized void addObserver(Observer o) {
		observerList.add(o);
	}
	
	@Override
	public void notifyObservers(Object arg) {
		Iterator<Observer> i = observerList.iterator();
		while (i.hasNext()) {
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					notifyObservers(ThreadyObservable.this);
				}
				
			}).start();
		}
	}
	
}
