package patterns.behavioral.observer;

public class Demo {
	public static void main(String[] args) {
		FireAlarm alarm = new FireAlarm();
		Students you = new Students();
		Teacher Sikkandar = new Teacher();
		alarm.addObserver(you);
		alarm.addObserver(Sikkandar);
		alarm.setAlert("Building in Fire");
	}
}
