package patterns.behavioral.observer;

import java.util.Observable;
import java.util.Observer;

public class Students implements Observer{
	
	String msg;

	@Override
	public void update(Observable o, Object arg) {
		this.msg = (String) arg;
		flyAway();
	}

	private void flyAway() {
		System.out.println(msg);
		System.out.println("Students are running . . . . . . . .");
	}

}
