package patterns.behavioral.memento;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("serial")
public class Gallery implements Serializable {
	
	private List<Picture> pics = new LinkedList<Picture>();
	
	public void add(Picture pic) {
		pics.add(pic);
	}
	
	public Picture remove(Picture pic) {
		pics.remove(pic);
		return pic;
	}
	
	public void display() {
		for (Picture pic : pics) {
			System.out.print(pic.getName() + "\t");
			System.out.println(pic.getModified().toString());
		}
	}

}
