package patterns.behavioral.memento;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class Picture implements Serializable {
	
	private String name;
	private Date modified;
	
	public Picture(String name) {
		this.name = name;
		this.modified = new Date();
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Date getModified() {
		return modified;
	}
	public void setModified(Date modified) {
		this.modified = modified;
	}

}
