package patterns.behavioral.memento;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Originator {

	public static boolean serialize(Object obj, String filename) {

		try {
			
			if (obj == null)
				return false;

			FileOutputStream fos = new FileOutputStream(filename);
			ObjectOutputStream oos = new ObjectOutputStream(fos);

			oos.writeObject(obj);

			oos.close();
			fos.close();

		} catch (Exception e) {

			return false;

		}
		
		return true;
	}
	
	public static Object deserialize(String filename) {
		
		Object obj;

		try {

			FileInputStream fis = new FileInputStream(filename);
			ObjectInputStream ois = new ObjectInputStream(fis);

			obj = ois.readObject();
			
			ois.close();
			fis.close();

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
		return obj;
	}

}
