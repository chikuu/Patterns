package patterns.behavioral.memento;

import java.util.Date;
import java.util.Deque;
import java.util.LinkedList;

public class Caretaker {
	
	private Deque<String> history = new LinkedList<String>();
	
	public boolean save(Object obj) {
		String filename = new String(new Date().getTime() + ".dat");
		if (Originator.serialize(obj, filename) == false)
			return false;
		else
			history.addLast(filename);
		
		while (history.size() > 3)
			history.removeFirst();

		return true;
	}
	
	public Object undo() {
		String filename = history.removeLast();
		return Originator.deserialize(filename);
	}
	
	public Object redo() {
		
		return null;
	}

}
