package patterns.behavioral.memento;

public class Demo {
	
	public static void main(String[] args) {
	
		Caretaker ct = new Caretaker();
		
		Gallery gallery = new Gallery();
		Picture pic1 = new Picture("Mountain");
		Picture pic2 = new Picture("Landscape");
		Picture pic3 = new Picture("River");
		
		gallery.add(pic1);
		gallery.add(pic2);
		gallery.add(pic3);

		
		ct.save(gallery);
		gallery.display();
		
		
		gallery.remove(pic1);
		gallery.remove(pic3);
		
		System.out.println("\nAfter deleting");
		
		gallery.display();
		
		System.out.println("\nRetrieving serialized obj");
		
		gallery = (Gallery)ct.undo();
		
		gallery.display();
		
		
	}

}
