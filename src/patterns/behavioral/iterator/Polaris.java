package patterns.behavioral.iterator;

public class Polaris extends Star {

	@Override
	public void about() {
		System.out.println("Yellow pole star");
	}

}
