package patterns.behavioral.iterator;

public abstract class Star {
	
	public abstract void about();

}
