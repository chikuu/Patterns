package patterns.structural.decorator;

public class Meat implements ShoppingItems {
	
	private ShoppingItems items;
	private float price = 40;

	public Meat(ShoppingItems items) {
		this.items = items;
	}

	@Override
	public float cost() {
		return items.cost() + price;
	}

}
