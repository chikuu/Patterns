package patterns.structural.decorator;

public class Honey implements ShoppingItems {
	
	private ShoppingItems items;
	private float price = 50;

	public Honey(ShoppingItems items) {
		this.items = items;
	}

	@Override
	public float cost() {
		return items.cost() + price;
	}

}
