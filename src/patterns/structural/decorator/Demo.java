package patterns.structural.decorator;

public class Demo {
	
	public static void main(String[] args) {

		ShoppingItems cart1 = new ShoppingCart();
		cart1 = new Milk(cart1);
		cart1 = new Honey(cart1);

		ShoppingItems cart2 = new ShoppingCart(cart1);
		cart2 = new Cheese(cart2);
		cart2 = new Bread(cart2);

		System.out.println(cart1.cost());
		System.out.println(cart2.cost());
	}
	
}
