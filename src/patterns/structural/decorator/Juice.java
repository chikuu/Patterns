package patterns.structural.decorator;

public class Juice implements ShoppingItems {
	
	private ShoppingItems items;
	private float price = 30;

	public Juice(ShoppingItems items) {
		this.items = items;
	}

	@Override
	public float cost() {
		return items.cost() + price;
	}

}
