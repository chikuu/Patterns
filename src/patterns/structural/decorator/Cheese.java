package patterns.structural.decorator;

public class Cheese implements ShoppingItems {
	
	private ShoppingItems items;
	private float price = 20;

	public Cheese(ShoppingItems items) {
		this.items = items;
	}

	@Override
	public float cost() {
		return items.cost() + price;
	}

}
