package patterns.structural.decorator;

public interface ShoppingItems {
	float cost();
}
