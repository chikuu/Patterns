package patterns.structural.decorator;

public class Eggs implements ShoppingItems {
	
	private ShoppingItems items;
	private float price = 60;

	public Eggs(ShoppingItems items) {
		this.items = items;
	}

	@Override
	public float cost() {
		return items.cost() + price;
	}

}
