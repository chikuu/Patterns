package patterns.structural.decorator;

public class Bread implements ShoppingItems {
	
	private ShoppingItems items;
	private float price = 30;
	
	public Bread(ShoppingItems items) {
		this.items = items;
	}

	@Override
	public float cost() {
		return items.cost() + price;
	}

}
