package patterns.structural.decorator;

public class ShoppingCart implements ShoppingItems {

	private ShoppingItems otherCart;
	
	public ShoppingCart() {	}
	
	public ShoppingCart(ShoppingItems otherCart) {
		this.otherCart = otherCart;
	}
	
	@Override
	public float cost() {
		if (otherCart == null)
			return 0;
		return otherCart.cost();
	}
	
}
