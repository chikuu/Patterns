package patterns.structural.decorator;

public class Milk implements ShoppingItems {
	
	private ShoppingItems items;
	private float price = 25.5f;
	
	public Milk(ShoppingItems items) {
		this.items = items;
	}

	@Override
	public float cost() {
		return items.cost() + price;
	}

}
