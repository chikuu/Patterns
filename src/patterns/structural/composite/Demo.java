package patterns.structural.composite;

public class Demo {
	
	public static void main(String[] args) {
		FScomponent root = new Directory("root");
		FScomponent home = new Directory("home");
		FScomponent var = new Directory("var");
		FScomponent cache = new Directory("cache");
		FScomponent user = new Directory("user");
		FScomponent music = new Directory("music");
		FScomponent pics = new Directory("pictures");
		FScomponent song1 = new File("Song1.mp3");
		FScomponent song2 = new File("Song2.mp3");
		FScomponent pic1 = new File("Image1.jpg");
		FScomponent pic2 = new File("Image2.jpg");
		FScomponent kern = new File("kernel-log.txt");
		
		var.add(kern);
		var.add(cache);
		music.add(song1);
		music.add(song2);
		pics.add(pic1);
		pics.add(pic2);
		user.add(music);
		user.add(pics);
		home.add(user);
		root.add(home);
		root.add(var);
		
		root.traverse();
	}

}
