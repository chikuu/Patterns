package patterns.structural.composite;

public abstract class FScomponent {
	private String name;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public abstract void share();
	public abstract void traverse();
	public abstract void add(FScomponent component);
}
