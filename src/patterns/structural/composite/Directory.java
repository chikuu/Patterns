package patterns.structural.composite;

import java.util.ArrayList;

public class Directory extends FScomponent {

	ArrayList<FScomponent> localComps = new ArrayList<FScomponent>();
	static int tabLength;
	
	public Directory(String name) {
		this.setName(name);
	}
	
	@Override
	public void share() {
		System.out.println("Directory sharing is currently unavailable");
	}

	@Override
	public void traverse() {
		
		for (int i = 0; i < tabLength; i++) {
			System.out.print("\t");
		}
		System.out.println(this.getName());
		++tabLength;
	
		for (FScomponent localComp : localComps) {
			if (localComp.getClass().getSimpleName().equalsIgnoreCase("Directory")) {
				localComp.traverse();
			} else if (localComp.getClass().getSimpleName().equalsIgnoreCase("File")) {
				for (int i = 0; i < tabLength; i++) {
					System.out.print("\t");
				}
				System.out.println(localComp.getName());
			}
		}
		--tabLength;
		
	}
	
	public void add(FScomponent component) {
		localComps.add(component);
	}

}
