package patterns.structural.composite;

public class File extends FScomponent {
	
	public File(String name) {
		this.setName(name);
	}

	@Override
	public void share() {
		System.out.println("Your file is shared :)");
	}

	@Override
	public void traverse() {
		System.out.println("Err!! File can't be traversed");
	}

	@Override
	public void add(FScomponent component) {
		System.out.println("Err!! Can't add to a file");
	}

}
